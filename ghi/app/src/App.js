import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState } from 'react';
import MainPage from './MainPage';
import HatList from './Hatlist';
import HatForm from './HatForm';
import Nav from './Nav';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App() {
  const [darkMode, setDarkMode] = useState(false);

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
    document.body.classList.toggle('dark-mode');
  };

  return (
    <BrowserRouter>
      <div>
        <Nav darkMode={darkMode} toggleDarkMode={toggleDarkMode} />
        <div className="container">
          <Routes>
            <Route path="/" element={<MainPage />} />
            <Route path="/shoes" element={<ShoeList darkMode={darkMode} />} />
            <Route path="/shoeform" element={<ShoeForm />} />
            <Route path="/hats" element={<HatList />} />
            <Route path="/hatform" element={<HatForm />} />
          </Routes>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;