import React, { useEffect, useState } from 'react';
import ShoeForm from './shoeform';
import './ShoeList.css';

function ShoeList() {
  const [shoes, setShoes] = useState([]);
  const [editingShoe, setEditingShoe] = useState(null);
  const [darkMode, setDarkMode] = useState(false);

  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8080/api/shoes/');
      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
      } else {
        throw new Error('Failed to fetch data');
      }
    } catch (error) {
      console.error('Error fetching shoe data:', error);
    }
  };

  const deleteShoe = async (shoeId) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this shoe?");
    if (!confirmDelete) return;

    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}`, { method: 'DELETE' });
      if (response.ok) {
        setShoes(shoes.filter(shoe => shoe.id !== shoeId));
      } else {
        throw new Error('Failed to delete shoe');
      }
    } catch (error) {
      console.error('Error deleting shoe:', error);
    }
  };

  const handleUpdateShoe = (shoe) => {
    setEditingShoe(shoe);
  };

  const handleFormSubmit = () => {
    setEditingShoe(null);
    getData();
  };

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
    document.body.classList.toggle('dark-mode');
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <div className="toggle-dark-mode-container">
        <button onClick={toggleDarkMode} className="toggle-dark-mode">
          {darkMode ? 'Light Mode' : 'Dark Mode'}
        </button>
      </div>

      {editingShoe ? (
        <ShoeForm existingShoe={editingShoe} onFormSubmit={handleFormSubmit} />
      ) : (
        <div className="shoe-list-container">
          {shoes.map((shoe) => (
            <div key={shoe.id} className="shoe-card">
              <img src={shoe.picture_url} className="card-img-top" alt={shoe.model_name} />
              <div className="card-body">
                <h5 className="card-title">{shoe.manufacturer} - {shoe.model_name}</h5>
                <p className="card-text">Color: {shoe.color}</p>
                <p className="card-text">Bin Number: {shoe.bin.bin_number}</p>
                <p className="card-text">Closet Name: {shoe.bin.closet_name}</p>
                <div className="button-group">
                  <button onClick={() => deleteShoe(shoe.id)} className="btn btn-delete">Delete</button>
                  <button onClick={() => handleUpdateShoe(shoe)} className="btn btn-update">Update</button>
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

export default ShoeList;
//is editing taht will hadnle true or false
//from there replace edditing shoe with isediringshoe
//udpate inthehanlde uodate shoe to toggle.isediring shoe so wehn we click it sets it to true
//pass set isediting shoe to shoeform as a prop to return boolean
//
//might need to use params when and if i decide to with url
//allows me to grab the id in the url that it is on and pass the shoes to the
