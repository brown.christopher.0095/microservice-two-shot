import React, { useState, useEffect } from 'react';
import HatForm from './HatForm';
import './HatList.css';

function HatList() {
  const [hats, setHats] = useState([]);
  const [editingHat, setEditingHat] = useState(null);
  const [darkMode, setDarkMode] = useState(false);

  const getData = async () => {
    try {
      const response = await fetch('http://localhost:8090/api/hats/');
      if (response.ok) {
        const data = await response.json();
        setHats(data.hats);
      } else {
        throw new Error('Failed to fetch hat data');
      }
    } catch (error) {
      console.error('Error fetching hat data:', error);
    }
  };

  const deleteHat = async (hatId) => {
    const confirmDelete = window.confirm("Are you sure you want to delete this hat?");
    if (!confirmDelete) return;

    try {
      const response = await fetch(`http://localhost:8090/api/hats/${hatId}`, { method: 'DELETE' });
      if (response.ok) {
        setHats(hats.filter(hat => hat.id !== hatId));
      } else {
        throw new Error('Failed to delete hat');
      }
    } catch (error) {
      console.error('Error deleting hat:', error);
    }
  };

  const handleUpdateHat = (hat) => {
    setEditingHat(hat);
  };

  const handleFormSubmit = () => {
    setEditingHat(null);
    getData();
  };

  const toggleDarkMode = () => {
    setDarkMode(!darkMode);
    document.body.classList.toggle('dark-mode');
  };

  useEffect(() => {
    getData();
  }, []);

  return (
    <div>
      <div className="toggle-dark-mode-container">
        <button onClick={toggleDarkMode} className="toggle-dark-mode">
          {darkMode ? 'Light Mode' : 'Dark Mode'}
        </button>
      </div>

      {editingHat ? (
        <HatForm existingHat={editingHat} onFormSubmit={handleFormSubmit} />
      ) : (
        <div className="hat-list-container">
          {hats.map((hat) => (
            <div key={hat.id} className="hat-card">
              <img src={hat.picture_url} className="card-img-top" alt={hat.style} />
              <div className="card-body">
                <h5 className="card-title">{hat.style} - {hat.color}</h5>
                <p className="card-text">Fabric: {hat.fabric}</p>
                <div className="button-group">
                  <button onClick={() => deleteHat(hat.id)} className="btn btn-delete">Delete</button>
                  <button onClick={() => handleUpdateHat(hat)} className="btn btn-update">Update</button>
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
}

export default HatList;