import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function ShoeForm({ existingShoe, onFormSubmit }) {
  const initialState = {
    manufacturer: '',
    model_name: '',
    color: '',
    picture_url: '',
    bin: '',
    id: null,
  };

  const [shoeData, setShoeData] = useState(initialState);
  const [bins, setBins] = useState([]);
  const [buttonText, setButtonText] = useState('Add Shoe');
  const navigate = useNavigate();

  useEffect(() => {
    const fetchBins = async () => {
      try {
        const response = await fetch('http://localhost:8100/api/bins/');
        if (response.ok) {
          const data = await response.json();
          setBins(data.bins);
        } else {
          throw new Error('Failed to fetch bins');
        }
      } catch (error) {
        console.error('Error fetching bins:', error);
      }
    };

    fetchBins();

    if (existingShoe) {
      setShoeData({
        manufacturer: existingShoe.manufacturer,
        model_name: existingShoe.model_name,
        color: existingShoe.color,
        picture_url: existingShoe.picture_url,
        bin: existingShoe.bin.href,
        id: existingShoe.id,
      });
      setButtonText('Update Shoe');
    } else {
      setShoeData(initialState);
      setButtonText('Add Shoe');
    }
  }, [existingShoe]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setShoeData(prevState => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    let shoeUrl;
    let method;

    if (shoeData.id) {
      shoeUrl = `http://localhost:8080/api/shoes/${shoeData.id}/`;
      method = "put";
    } else {
      shoeUrl = 'http://localhost:8080/api/shoes/';
      method = "post";
    }

    const fetchConfig = {
      method: method,
      body: JSON.stringify(shoeData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(shoeUrl, fetchConfig);
      if (response.ok) {
        setShoeData(initialState);
        if (onFormSubmit) {
          onFormSubmit();
        }
        navigate('/shoes');
      } else {
        console.error("HTTP error:", response.status);
      }
    } catch (error) {
      console.error("Network error:", error);
    }
  };
//position an icon button for toggle.isEditingShoeStae()
//re-render the page
  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="manufacturer" className="form-label">Manufacturer</label>
          <input type="text" className="form-control" id="manufacturer" name="manufacturer" value={shoeData.manufacturer} onChange={handleInputChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="model_name" className="form-label">Model Name</label>
          <input type="text" className="form-control" id="model_name" name="model_name" value={shoeData.model_name} onChange={handleInputChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="color" className="form-label">Color</label>
          <input type="text" className="form-control" id="color" name="color" value={shoeData.color} onChange={handleInputChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="picture_url" className="form-label">Picture URL</label>
          <input type="text" className="form-control" id="picture_url" name="picture_url" value={shoeData.picture_url} onChange={handleInputChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="bin" className="form-label">Bin</label>
          <select className="form-select" id="bin" name="bin" value={shoeData.bin} onChange={handleInputChange} required>
            <option value="">Select a Bin</option>
            {bins.map(bin => (
              <option key={bin.id} value={bin.href}>{bin.closet_name}</option>
            ))}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">{buttonText}</button>
      </form>
    </div>
  );
}

export default ShoeForm;
