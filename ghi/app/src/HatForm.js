import React, { useState, useEffect, useMemo } from 'react';
import { useNavigate } from 'react-router-dom';

function HatForm({ existingHat, onFormSubmit }) {
  const initialState = useMemo(() => ({
    fabric: '',
    color: '',
    style: '',
    picture_url: '',
    id: null,
  }), []);

  const [hatData, setHatData] = useState(initialState);
  const [hatImages, setHatImages] = useState([]);
  const [buttonText, setButtonText] = useState('Add Hat');
  const navigate = useNavigate();

  useEffect(() => {
    const fetchHatImages = async () => {
      try {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
          const data = await response.json();
          setHatImages(data.hatImages);
        } else {
          throw new Error('Failed to fetch hat images');
        }
      } catch (error) {
        console.error('Error fetching hat images:', error);
      }
    };

    fetchHatImages();

    if (existingHat) {
      setHatData({
        fabric: existingHat.fabric,
        color: existingHat.color,
        style: existingHat.style,
        picture_url: existingHat.picture_url,
        id: existingHat.id,
      });
      setButtonText('Update Hat');
    } else {
      setHatData(initialState);
      setButtonText('Add Hat');
    }
  }, [existingHat, initialState]);

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setHatData((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    let hatUrl;
    let method;

    if (hatData.id) {
      hatUrl = `http://localhost:8090/api/hats/${hatData.id}/`;
      method = 'put';
    } else {
      hatUrl = 'http://localhost:8090/api/hats/';
      method = 'post';
    }

    const fetchConfig = {
      method: method,
      body: JSON.stringify(hatData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(hatUrl, fetchConfig);
      if (response.ok) {
        setHatData(initialState);
        if (onFormSubmit) {
          onFormSubmit();
        }
        navigate('/hats');
      } else {
        console.error('HTTP error:', response.status);
      }
    } catch (error) {
      console.error('Network error:', error);
    }
  };

  return (
    <div className="container">
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="fabric" className="form-label">
            Fabric
          </label>
          <input
            type="text"
            className="form-control"
            id="fabric"
            name="fabric"
            value={hatData.fabric}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="color" className="form-label">
            Color
          </label>
          <input
            type="text"
            className="form-control"
            id="color"
            name="color"
            value={hatData.color}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="style" className="form-label">
            Style
          </label>
          <input
            type="text"
            className="form-control"
            id="style"
            name="style"
            value={hatData.style}
            onChange={handleInputChange}
            required
          />
        </div>
        <div className="mb-3">
          <label htmlFor="picture_url" className="form-label">
            Picture URL
          </label>
          <select
            className="form-select"
            id="picture_url"
            name="picture_url"
            value={hatData.picture_url}
            onChange={handleInputChange}
            required
          >
            <option value="">Select an Image</option>
            {hatImages.map((image) => (
              <option key={image.id} value={image.url}>
                {image.name}
              </option>
            ))}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">
          {buttonText}
        </button>
      </form>
    </div>
  );
}

export default HatForm;