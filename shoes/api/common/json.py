from json import JSONEncoder
from typing import Any
from django.urls import NoReverseMatch
from django.db.models import QuerySet, Model
from datetime import datetime
from shoes_rest.models import Shoe

class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {}

    def default(self, o):
        if isinstance(o, self.model):
            d = {}
            if hasattr(o, "get_api_url"):
                try:
                    d["href"] = o.get_api_url()
                except NoReverseMatch:
                    pass
            for property in self.properties:
                value = getattr(o, property)
                if property in self.encoders:
                    encoder = self.encoders[property]
                    value = encoder.default(value)
                d[property] = value
            d.update(self.get_extra_data(o))
            return d
        else:
            return super().default(o)

    def get_extra_data(self, o):
        return {}

# class ShoeEncoder(JSONEncoder):
#     def default(self,o):
#         if isinstance(o,Shoe):
#             return {
#                 "id":o.id,
#                 "manufacturer": o.manufacturer,
#                 "model_name": o.model_name,
#                 "color":o.color,
#                 "picture_url":o.picture_url,
#                 "bin":o.bin

#             }
#     def get_extra_data(self,o):
#         return {}

# class ShoeListEncoder(JSONEncoder):
#     def default(self,o):
#         if isinstance(o, QuerySet):
#             return [self.encode_shoe(shoe) for shoe in o]
#         return super().default(o)

#     def encode(self, shoe):
#         return {
#             "id": shoe.id,
#             "manufacturer": shoe.manufacturer,
#             "model_name": shoe.model_name,
#             "color": shoe.color,
#             "picture_url": shoe.picture_url,
#             "bin": shoe.bin
#         }
