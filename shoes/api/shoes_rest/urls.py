from django.urls import path
from shoes_rest.views import api_Show_shoe, api_List_shoes

urlpatterns = [
    path('shoes/', api_List_shoes, name='shoe_list'),
    path("shoes/", api_List_shoes, name="create_shoes"),
    path('shoes/<int:id>/', api_Show_shoe,name='shoe_detail'),
]
