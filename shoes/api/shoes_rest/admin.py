from django.contrib import admin
from .models import Shoe
# Register your models here.



@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = ('manufacturer', 'model_name', 'color', 'bin')
    search_fields = ('manufacturer', 'model_name', 'color', 'bin__closet_name')
