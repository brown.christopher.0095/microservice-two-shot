from django.shortcuts import render
from django.http import  JsonResponse
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        'closet_name',
        'bin_number',
        'bin_size',
        'import_href'
    ]

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        'bin': BinVODetailEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin"
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_List_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse({"shoes": shoes}, safe=False,encoder=ShoeListEncoder)
    else:
        try:
            content = json.loads(request.body)
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content['bin'] = bin
            shoe = Shoe.objects.create(**content)
        except BinVO.DoesNotExist:
            return JsonResponse({'message': 'Bin does not exist with the id'}, status=400)

        return JsonResponse(
            shoe,
            encoder=ShoeListEncoder,
            safe=False
        )




@require_http_methods(["DELETE", "GET", "PUT"])
def api_Show_shoe(request, id):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=id)
            return JsonResponse(shoe,safe=False,encoder=ShoeDetailEncoder)
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"}, status=404)
    elif request.method == "DELETE":
        try:
            shoe = Shoe.objects.get(id=id)
            shoe.delete()
            return JsonResponse({"message": "Shoe deleted"})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"}, status=404)
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            shoe = Shoe.objects.get(id=id)

            props = ["manufacturer", "model_name", "color", "picture_url", "bin_id"]
            for prop in props:
                if prop in content:
                    setattr(shoe, prop, content[prop])
            shoe.save()
            return JsonResponse(shoe,safe=False, encoder=ShoeDetailEncoder)
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Shoe does not exist"}, status=404)
