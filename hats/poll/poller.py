import django
import os
import sys
import time
import json
import requests

sys.path.append("")  
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO

def get_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    data = json.loads(response.content)
    
    locations = data.get('locations', [])  
    
    for location in locations:
        closet_name = location.get("closet_name")
        section_number = int(location.get("section_number"))
        shelf_number = int(location.get("shelf_number"))

        LocationVO.objects.update_or_create(
            closet_name=closet_name, 
            section_number=section_number, 
            shelf_number=shelf_number
        )

def poll():
    while True:
        try:
            get_locations()
        except Exception as e:
            print(e)
        time.sleep(60) 

if __name__ == "__main__":
    poll()