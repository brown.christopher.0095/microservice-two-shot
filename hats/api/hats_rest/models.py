from django.db import models
from django.urls import reverse

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.closet_name


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    style = models.CharField(max_length=100)
    picture_url = models.URLField()
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return f"{self.style} - {self.color}"