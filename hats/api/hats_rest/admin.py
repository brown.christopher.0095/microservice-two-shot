from django.contrib import admin
from .models import Hat, LocationVO
# Register your models here.


@admin.register(Hat)
class HatAdmin(admin.ModelAdmin):
    list_display = ('fabric', 'color', 'style', 'location')


class LocationVOAdmin(admin.ModelAdmin):
    list_display = ('closet_name', 'section_number', 'shelf_number')