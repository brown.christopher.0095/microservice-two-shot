from django.urls import path
from . import views  # Ensure views are imported from the current app

urlpatterns = [
    path('hats/', views.hat_list, name='hat_list'),
    path('hats/add/', views.create_hat, name='create_hat'),
    path('hats/delete/<int:id>/', views.delete_hat, name='delete_hat'),
    path('locations/', views.location_list, name='location_list'),
    path('locations/<int:pk>/', views.location_detail, name='location_detail'),
]