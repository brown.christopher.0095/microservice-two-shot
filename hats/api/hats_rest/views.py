from django.http import JsonResponse, HttpResponse  
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO
from common.json import ModelEncoder



class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name", 
        "section_number", 
        "shelf_number",
        ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "color", 
        "style", 
        "picture_url", 
        "location",
        ]
    encoders = {
        "locations": LocationVOEncoder(),
    }

class LocationDetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name", 
        "section_number", 
        "shelf_number",
    ]

@require_http_methods(["DELETE", "GET", "PUT"])
def location_detail(request, pk):
    try:
        location = LocationVO.objects.get(id=pk)
    except LocationVO.DoesNotExist:
        return HttpResponse(status=404)

    if request.method == "GET":
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = LocationVO.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})

    elif request.method == "PUT":
        content = json.loads(request.body)
        for field, value in content.items():
            if hasattr(location, field):
                setattr(location, field, value)
        location.save()
        return JsonResponse(location, encoder=LocationDetailEncoder, safe=False)

    else:
        return HttpResponse(status=405)

@require_http_methods(["GET", "POST"])
def location_list(request):
    locations = LocationVO.objects.all()
    return JsonResponse(list(locations), encoder=LocationVOEncoder, safe=False)

@require_http_methods(["GET"])
def hat_list(request):
    if request.method == "GET":
        hats = Hat.objects.select_related('location').all()

        list_hats = [
            {
                "fabric": hat.fabric,
                "color": hat.color,
                "style": hat.style,
                "picture_url": hat.picture_url,
                "location": {
                    "closet_name": hat.location.closet_name,
                    "section_number": hat.location.section_number,
                    "shelf_number": hat.location.shelf_number,
                }
            }
            for hat in hats
        ]

        return JsonResponse(list_hats, safe=False)


@require_http_methods(["POST"])
def create_hat(request):
    if request.method == 'POST':
        try:
            data = json.loads(request.body)
            location = LocationVO.objects.get(id=data['location_id'])
            hat = Hat.objects.create(
                fabric=data['fabric'],
                color=data['color'],
                style=data['style'],
                picture_url=data['picture_url'],
                location=location
            )
            return JsonResponse({'id': hat.id}, status=201)
        except Exception as e:
            return HttpResponse(status=400)
    else:
        return HttpResponse(status=405)


@require_http_methods(["DELETE"])
def delete_hat(request, id):
    try:
        hat = Hat.objects.get(id=id)
        hat.delete()
        return HttpResponse(status=204)
    except Hat.DoesNotExist:
        return HttpResponse(status=404)
    except Exception as e:
        return HttpResponse(status=400)
    





